﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FlightsFinal
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public  Window1()
        {

            InitializeComponent();
            
           
            
            comboType.ItemsSource = Enum.GetValues(typeof(Flight.Etype)).Cast<Flight.Etype>();
            comboType.SelectedIndex = 0;
            
        }

        

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try { 
            DateTime date = dpDate.SelectedDate.Value;
            string toCode = tbTo.Text;
            string fromCode= tbFrom.Text;
            int passengers = Convert.ToInt32(slPassengers.Value);
            var type = (Flight.Etype)comboType.SelectedItem;
            }
            catch (InvalidDataException ex)
            {
                MessageBox.Show(this, "Ignoring invalid line.\n" + ex.Message, "File data error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (dpDate.SelectedDate == null)
            {
                MessageBox.Show(this, "Please select a date", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }


            //Flight flight = new Flight(date, fromCode, toCode, passengers, type);
           
            
            DialogResult = true; 

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
