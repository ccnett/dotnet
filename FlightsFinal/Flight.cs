﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;

namespace FlightsFinal
{
    public class InvalidDataException : Exception
    {
        public InvalidDataException(string msg) : base(msg) { }
        public InvalidDataException(string msg, Exception inner) : base(msg, inner) { }
    }
    class Flight
    {
        //DateTime Date;
        //string FromCode;
        //string ToCode;
        
        //int Passengers;
        //public Etype Type;

        public Flight(string dataline)
        {
            string[] data = dataline.Split(';');
            if(data.Length != 5)
            {
                throw new InvalidDataException("Invalid number of elements in line");
            }
            Date = DateTime.ParseExact(data[0], "yyyy-MM-dd", CultureInfo.InvariantCulture);
            Type = (Etype)Enum.Parse(typeof(Etype), data[1], true);
            FromCode = data[2];
            ToCode = data[3];
            Passengers = Int32.Parse(data[4]);
            
        }

        public Flight(DateTime date, string fromCode, string toCode, int passengers, Etype type)
        {
            Date = date;
            FromCode = fromCode;
            ToCode = toCode;
            Passengers = passengers;
            Type = type;


        }
        private DateTime _date;
        public DateTime Date
        {
            get
            {
                return _date ;
            }
            set
            {
                _date = value.Date.Date;
            }
        }
        private string _fromCode;
        public String FromCode
        {
            get
            {
                return _fromCode;
            }
            set
            {
                if (!Regex.IsMatch(value, @"^[A-Z]{3,5}$"))
                {
                    
                    
                    throw new InvalidDataException("Invalid airport code in From field");
                    
                }
                _fromCode = value;
            }
        }
        private string _toCode;
        public String ToCode
        {
            get
            {
                return _toCode;
            }
            set
            {
                if (!Regex.IsMatch(value, @"^[A-Z]{3,5}$"))
                {
                    throw new InvalidDataException("Invalid aiport code in To field");
                }
                    _toCode = value;
            }
        }
        private int _passengers;
        public int Passengers
        {
            get
            {
                return _passengers;
            }
            set
            {
                _passengers = value;
            }
        }
        private Etype _type;
        public Etype Type
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
            }
        }



        public enum Etype {Domestic,Internationnal,Private }
        public override string ToString()
        {
            return $"{Date:yyy-MM-dd},{Type},{ToCode},{FromCode},{Passengers}";
        }
        public string ToDataString()
        {
            return $"{Date:yyy-MM-dd};{Type};{ToCode};{FromCode};{Passengers}";
        }



    }

}
