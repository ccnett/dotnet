﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FlightsFinal
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const string DataFileName = @"..\..\flights.txt";
        private List<Flight> flightList = new List<Flight>();

        public MainWindow()
        {
            LoadDataFromFile();
            InitializeComponent();
            lvFlights.ItemsSource = flightList;
            lblCursorPosition.Text = $"{flightList.Count} Flights";
            lvFlights.Items.Refresh();
        }

        private void SaveDataToFile(string fileName, List<Flight> flightSave)
        {
            try
            {
                List<string> linesList = new List<string>();
                foreach (Flight f in flightSave)
                {
                    linesList.Add(f.ToDataString());
                }
                File.WriteAllLines(fileName, linesList);
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                MessageBox.Show(this, ex.Message, "File IO error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }




        private void LoadDataFromFile()
        {
            try
            {
                if (!File.Exists(DataFileName)) return; 
                string[] linesList = File.ReadAllLines(DataFileName);
                foreach (string line in linesList)
                {
                    try
                    {
                        Flight flight = new Flight(line); 
                        flightList.Add(flight);
                    }
                    catch (InvalidDataException ex)
                    {
                        MessageBox.Show(this, "Ignoring invalid line.\n" + ex.Message, "File data error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                File.WriteAllLines(@"..\..\data.txt", linesList);
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                MessageBox.Show(this, ex.Message, "File IO error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }


        private void miEditAdd_Click(object sender, RoutedEventArgs e)
        {
            Window1 dlg = new Window1();
            dlg.Owner = this;
            if (dlg.ShowDialog() == true)
            {
                try
                {
                    DateTime date = dlg.dpDate.SelectedDate.Value;
                    string toCode = (dlg.tbTo.Text).ToUpper();
                    string fromCode = (dlg.tbFrom.Text).ToUpper();

                    int passengers = Convert.ToInt32(dlg.slPassengers.Value);
                    var type = (Flight.Etype)dlg.comboType.SelectedItem;
                    
                    Flight flight = new Flight(date.Date, fromCode, toCode, passengers, type);

                }
                catch (InvalidDataException ex)
                {
                    MessageBox.Show(this, "Error\n" + ex.Message, "File data error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                
                lvFlights.Items.Refresh();
                lblCursorPosition.Text = $"{flightList.Count}+ Flights";
            }
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            SaveDataToFile(DataFileName, flightList);
        }

        private void MenuItem_Exit(object sender, RoutedEventArgs e)
        {
            SaveDataToFile(DataFileName, flightList);
            App.Current.MainWindow.Close();
        }
        private void miSaveSelected(object sender, RoutedEventArgs e)
        {
            var selItems = lvFlights.SelectedItems.Cast<Flight>().ToList();
            if (selItems.Count == 0)
            {
                MessageBox.Show(this, "Please select some items first", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog.FilterIndex = 1;
            // saveFileDialog1.RestoreDirectory = true;
            if (saveFileDialog.ShowDialog() == true)
            {
                string fileName = saveFileDialog.FileName;
                SaveDataToFile(fileName, selItems);
                lblCursorPosition.Text = "Selected Item(s) exported";
            }
        }
        
            private void lvFlights_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            
            
            Window1 dlg = new Window1();
            dlg.Owner = this;
            dlg.Show();
            dlg.dlgButton.Content = "Update";
            dlg.dpDate.SelectedDate = flightList[lvFlights.SelectedIndex].Date;
            dlg.tbTo.Text = flightList[lvFlights.SelectedIndex].ToCode;
            dlg.tbFrom.Text = flightList[lvFlights.SelectedIndex].FromCode;
            dlg.slPassengers.Value = flightList[lvFlights.SelectedIndex].Passengers;
            dlg.comboType.SelectedItem = flightList[lvFlights.SelectedIndex].Type;
            
            
            
        }
        private void ContextMenuItem_Click_Edit(object sender, RoutedEventArgs e)
        {
            Window1 dlg = new Window1();
            dlg.Owner = this;
            dlg.Show();
            dlg.dlgButton.Content = "Update";
            dlg.dpDate.SelectedDate = flightList[lvFlights.SelectedIndex].Date;
            dlg.tbTo.Text = flightList[lvFlights.SelectedIndex].ToCode;
            dlg.tbFrom.Text = flightList[lvFlights.SelectedIndex].FromCode;
            dlg.slPassengers.Value = flightList[lvFlights.SelectedIndex].Passengers;
            dlg.comboType.SelectedItem = flightList[lvFlights.SelectedIndex].Type;
            
        }
        private void ContextMenuItem_Click_Delete(object sender, RoutedEventArgs e)
        {
           flightList.Remove(flightList[lvFlights.SelectedIndex]);
            lvFlights.Items.Refresh();

        }



    }
}
