﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("What is your name?");
            string userName = Console.ReadLine();
            Console.WriteLine("What is your age?");
            try { 
            int userAge = Convert.ToInt32(Console.ReadLine());
            if(userAge<1|| userAge > 100)
            {
                Console.WriteLine("Age must be between 1 and 100");
                    return;
            }
                Console.WriteLine("User's name is: " + userName + "And he is: " + userAge + " years old");
                Console.WriteLine("Press any key to exit");


            }
            catch(System.FormatException e)
            {
                Console.WriteLine("Age must be interger");
            }
            
        }
    }
}
