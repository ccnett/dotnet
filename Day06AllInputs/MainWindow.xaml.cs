﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day06AllInputs
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public class Person
    {
        string name;
        string age;
        string pets;
        string continent;
        double prefTemp;
        private object age1;
        private List<string> petList;
        private object continent1;
        private double prefTemp1;

        public Person(string name, string age, string pets, string continent, double prefTemp)
        {
            this.name = name;
            this.age = age;
            this.pets = pets;
            this.continent = continent;
            this.prefTemp = prefTemp;
        }

        
       
    }
    
    
   
    public partial class MainWindow : Window
    {
        public List<Person> peopleList = new List<Person>();
        public MainWindow()
        {
            InitializeComponent();
        }
        
        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var name = tfName.Text;
             
            var prefTemp = sliderTempC.Value;
            List<string> petList = new List<string>();
           
            if ((bool)chCat.IsChecked)
            {
                petList.Add("Cat");
            }
            if ((bool)chDog.IsChecked)
            {
                petList.Add("Cat");
            }
            if ((bool)chOther.IsChecked)
            {
                petList.Add("Cat");
            }

            
           
            if ((bool)rdBelow.IsChecked, out string ageVal)
            {
                string ageVal = "Below 18";
            }
            else if ((bool)rdThirtyfive.IsChecked, out string ageVal)
            {
                string ageVal = "18- 35";
            }
            else if ((bool)rdAndUp.IsChecked, out string ageVal)
            {
                string ageVal = "36 +";
            }
            else
            {
                MessageBox.Show("ERROR");
            }
            

            string continent = cbContinent.SelectedValue?.ToString();

            string pets = string.Join<string>(",", petList);

            var age = ageVal;

            Person person = new Person(name, age, pets, continent, prefTemp);
            
            MessageBox.Show(person.ToString());


            
        }

        private void RadioButtonChecked(object sender, RoutedEventArgs e)
        {
            var radioButton = sender as RadioButton;
            if (radioButton == null)
                return;
           string ageSelect = radioButton.Content.ToString();
            
        }
    }
}
