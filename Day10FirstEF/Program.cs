﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day10FirstEF
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                SocietyDbContext ctx = new SocietyDbContext();
                Random random = new Random();
                Person p1 = new Person { Name = "Jerry" + random.Next(100), Age = random.Next(100) };
                ctx.People.Add(p1);
                ctx.SaveChanges();
                Console.WriteLine("record added");
                Person p2 = (from p in ctx.People where p.Id == 2 select p).FirstOrDefault<Person>();
                if (p2 != null)
                {
                    p2.Name = "Alabama" + (random.Next(10000) + 10000);
                    ctx.SaveChanges();
                    Console.WriteLine("record added");
                }
                else
                {
                    Console.WriteLine("record to update not found");
                }
                //delete
                Person p3 = (from p in ctx.People where p.Id == 3 select p).FirstOrDefault<Person>();
                if (p3 != null)
                {
                    ctx.People.Remove(p3);
                    ctx.SaveChanges();
                    Console.WriteLine("record deleted");
                }
            }
            catch(SystemException ex)
            {
                Console.WriteLine("DB operation failed: " + ex.Message);
            }
            
            finally
            {
                Console.WriteLine("Press any key");
                Console.ReadKey();
            }
        }


    }
}
