﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day11CarOwnersEF
{
    /// <summary>
    /// Interaction logic for CarsDialog.xaml
    /// </summary>
    public partial class CarsDialog : Window
    {
        CarsOwnersDbContext ctx;
        public Car currCar;

        public CarsDialog(Car tc = null)
        {
            try
            {

                InitializeComponent();
                ctx = new CarsOwnersDbContext();
                lvCarList.ItemsSource = (from c in ctx.Cars select c).ToList<Car>();
                if(tc != null)
                {
                    currCar = tc;
                    tbcarMake.Text = tc.CarMakeModel;
                }
                
            }
            catch (SystemException ex) // catch-all for EF, SQL and many other exceptions
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Fatal error: Database connection failed:\n" + ex.Message);
                Environment.Exit(1); // fatal error
            }
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {

            string makeModel = tbcarMake.Text;
           
            currCar.CarMakeModel = makeModel;


            DialogResult = true;
        }
    }
}
