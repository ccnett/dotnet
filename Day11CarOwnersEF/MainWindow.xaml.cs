﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day11CarOwnersEF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        CarsOwnersDbContext ctx;
        private byte[] currOwnerImage;

        public MainWindow()
        {
            try
            {

                InitializeComponent();
            ctx = new CarsOwnersDbContext();
            lvOwners.ItemsSource = (from o in ctx.Owners select o).ToList<Owner>();
            }
            catch (SystemException ex) // catch-all for EF, SQL and many other exceptions
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Fatal error: Database connection failed:\n" + ex.Message);
                Environment.Exit(1); // fatal error
            }

        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            
            
            List<Car> carList = new List<Car>();
            
            Owner owner = new Owner();
            owner.Name = tbName.Text;
            owner.Pic = currOwnerImage;



            ctx.Owners.Add(owner);
            ctx.SaveChanges();
            lvOwners.ItemsSource = (from o in ctx.Owners select o).ToList<Owner>();
        }

        private void btPic_Click(object sender, RoutedEventArgs e)
        {
            var fileContent = string.Empty;
            var filePath = string.Empty;

            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.InitialDirectory = "c:\\";
            openFileDialog.Filter = "Image Files|*.jpg;*.jpeg;*.png;|All files (*.*)|*.*";
            openFileDialog.FilterIndex = 1;
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == true)
            {
                //Get the path 
                filePath = openFileDialog.FileName;

                //Read the contents of the file into a stream
                var fileStream = openFileDialog.OpenFile();

                
                
                byte[] img = imageToByteArray(Image.FromFile(filePath));


                var result = MessageBox.Show(fileContent, "Are you Sure you want to import \"" + System.IO.Path.GetFileName(openFileDialog.FileName) + "\" ?", MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (result == MessageBoxResult.Yes)
                {
                    try
                    {
                        var currOwnerImage = File.ReadAllBytes(filePath);
                        tbImage.Visibility = Visibility.Hidden; // hide the text box
                        BitmapImage bitmap = LoadImage(img); // ex: SystemException
                        imagePreview.Source = bitmap;
                    }
                    catch (Exception ex) when (ex is SystemException || ex is IOException)
                    {
                        MessageBox.Show(ex.Message, "File reading failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }

                }
            }

        }
       
        private static BitmapImage LoadImage(byte[] imageData)
        {
            if (imageData == null || imageData.Length == 0) return null;
            var image = new BitmapImage();
            using (var mem = new MemoryStream(imageData))
            {
                mem.Position = 0;
                image.BeginInit();
                image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.UriSource = null;
                image.StreamSource = mem;
                image.EndInit();
            }
            image.Freeze();
            return image;
        }
        public byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            return ms.ToArray();
        }
        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }

        private void btManage_Click(object sender, RoutedEventArgs e)
        {

            if(lvOwners.SelectedValue != null)
            {
                CarsDialog cDlg = new CarsDialog();
                var selectedOwner = lvOwners.SelectedItem;
                cDlg.Owner = this;
                if (cDlg.ShowDialog() == true)
                {
                    List<Car> carList;
                    cDlg.lblowner.Content = selectedOwner;
                    if(cDlg.lvCarList.SelectedItem != null) { 
                    cDlg.lblcar.Content = cDlg.lvCarList.SelectedItem;
                    }

                    Car car = new Car();
                    car.CarMakeModel = cDlg.currCar.CarMakeModel;
                    ctx.Cars.Add(car);
                    ctx.SaveChanges();

                    lvOwners.ItemsSource = (from o in ctx.Owners select o).ToList<Owner>();


                }
            }
            else
            {
                MessageBox.Show("You Need To Select An Owner To See Cars", "Error Message");
            }
            
        }

        private void lvOwners_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {

        }
    }
    }



