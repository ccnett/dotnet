﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day11CarOwnersEF
{
     public class Owner
    {
       public int OwnerId { get; set; }
        [Required]
       public string Name { get; set; }
       public  byte[] Pic { get; set; }
        
        [NotMapped]
       public int CarNumber
        {
            get
            {
                return CarList.Count();
            }
        }
        public virtual ICollection<Car> CarList { get; set; }


    }

    public class Car
    {
        
        
      
        public int CarID { get; set; }

        public int OwnerId;

        [Required]
        public string CarMakeModel { get; set; }

    }

}
