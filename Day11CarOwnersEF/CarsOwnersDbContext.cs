using System;
using System.Data.Entity;
using System.IO;
using System.Linq;

namespace Day11CarOwnersEF
{
    public class CarsOwnersDbContext : DbContext
    {
        const string DbName = "DatabaseFilename.mdf";

        static string DbPath = Path.Combine(Environment.CurrentDirectory + @"\..\..\", DbName);

        public CarsOwnersDbContext() : base($@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename={DbPath};Integrated Security=True;Connect Timeout=30") { }

        public virtual DbSet<Car> Cars { get; set; } // must be virtual

        public virtual DbSet<Owner> Owners { get; set; } // must be virtual

    }
}