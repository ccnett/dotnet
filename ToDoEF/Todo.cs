﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoEF
{
    public class InvalidDataException : Exception
    {
        public InvalidDataException(string msg) : base(msg) { }
        public InvalidDataException(string msg, Exception inner) : base(msg, inner) { }
    }
    public class Todo
    {
        
        int Id { get; set; }
        [Required]
         String Task; // 1-100 characters, made up of uppercase and lowercase letters, digits, space, %_-(),./\ only
        [Required]
        int Difficulty; // 1-5, as slider
        [Required]
        DateTime DueDate; // year 1900-2100 both inclusive, use formatted field
        [Required]
        public EStatus Status; // define an enumeration - one of: Pending, Done, Delegated - matches the ComboBox in GUI

        public  Todo(int id, string task,int difficulty,DateTime duedate,EStatus status)
        {
            Id = id;
            Task = task;
            Difficulty = difficulty;
            DueDate = duedate;
            Status = status;
        }

    }

    public enum EStatus { Pending, Done, Delegated};
}

