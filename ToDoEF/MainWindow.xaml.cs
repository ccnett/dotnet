﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ToDoEF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        TodDBContext ctx;
        public MainWindow()
        {
            try { 
            InitializeComponent();
            ctx = new TodDBContext();
            }
            catch(SystemException ex)
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Database connection failed: " + ex.Message);
                Environment.Exit(1);
            }
        }

        private void btAddTodo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Todo t = new Todo(0,"new task",  2, DateTime.Now, EStatus.Pending );
            ctx.Todos.Add(t);
            ctx.SaveChanges();
            lvTripList.ItemsSource = (from u in ctx.Todos select u).ToList<Todo>();

            }
            catch (SystemException ex) // catch-all for EF, SQL and many other exceptions
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Database operation failed:\n" + ex.Message);
            }



        }

        private void btUpdateTodo_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btDeleteTodo_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click_Save(object sender, RoutedEventArgs e)
        {

        }

        private void lvTripList_MouseDoubleClick(object sender, MouseAction e)
        {

        }
    }
}
