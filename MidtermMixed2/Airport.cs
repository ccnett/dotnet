﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MidtermMixed2
{
    
    public class Airport
    {
        // string Code;
        string City;
        // double Latitude;
        //double Longitude;
        int ElevationMeters;

       
        
        public Airport(string code,string city, double latitude, double longitude, int elevationMeters)
        {

            Code = code;
            City = city;
            Latitude = latitude;
            Longitude = longitude;
            ElevationMeters = elevationMeters;


        }
        private string _code;
        public string Code
        {
            get
            {
                return _code;
            }
            set
            {
               
                _code = value;
            }

        }
        private  double _lat;

        public double Latitude
        {
            get
            {
                return _lat;
            }
            set
            {
               if(value < -90 || value > 90)
                {
                    Logging.LogFailure?.Invoke("LAT must be Valid");
                    throw new OtherClasses.InvalidParameterException("LAT must be Valid");
                }
                _lat = value;
            }
           
        }
        private double _long;
        public double Longitude
        {
            get
            {
                return _long;
            }
            set
            {
                if (value < -180 || value > 180)
                {
                    Logging.LogFailure?.Invoke("LONG must be Valid");
                    throw new OtherClasses.InvalidParameterException("LONG must be Valid");
                }
                _lat = value;
            }
        }

        public Airport(string dataLine)
        {
            string airportCodePat = @"/^[A-Z]{3}$/";
            
            Regex codeCheck = new Regex(airportCodePat);
            
            string[] data = dataLine.Split(';');
            if (data.Length != 5)
            {
                Logging.LogFailure?.Invoke("Person data line must have 5 fields");
                throw new OtherClasses.InvalidParameterException("Person data line must have 5 fields");
            }
            if (data[0].Length!=3 ){
                Logging.LogFailure?.Invoke("Airport Code Should Consist of 3 letters");
                throw new OtherClasses.InvalidParameterException("Airport Code Should Consist of 3 letters");
            }
            else
            {
                Code= data[0];
            }
            City = data[1];
            
            if(!double.TryParse(data[2],out double latitude))
            {
                Logging.LogFailure?.Invoke("Not Valid Latitude");
                throw new OtherClasses.InvalidParameterException("Not Valid Ltitude");
            }
            Latitude = latitude;
            
            if (!double.TryParse(data[3], out double longitude))
            {
                Logging.LogFailure?.Invoke("Not Valid Ltitude");
                throw new OtherClasses.InvalidParameterException("Not Valid Ltitude");
            }
            Longitude = longitude;
            if (!int.TryParse(data[4], out int elevationMeters))
            {
                Logging.LogFailure?.Invoke("Not Valid Ltitude");
                throw new OtherClasses.InvalidParameterException("Not Valid Ltitude");
            }
            ElevationMeters = elevationMeters;
        }
        public override string ToString()
        {
            return $"{Code}=>{City} Coordinates:{Latitude}/{Longitude} at {ElevationMeters} Meters above sea Level";
        }

        public virtual string ToDataString()
        {
            return $"{Code};{City};{Latitude};{Longitude};{ElevationMeters}";
        }
    }
}
