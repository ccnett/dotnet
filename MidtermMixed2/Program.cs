﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidtermMixed2
{
    class Program
    {
        const string DataFileName = @"..\..\airports.txt";
       
        static void ListAllAirports()
        {
            ReadAirportFile();
            Console.WriteLine("Listing all Airports");
            foreach (Airport airport in AirportList)
            {
                Console.WriteLine(airport);
            }
        }
        static int getMenuChoice()
        {
            while (true)
            {
                Console.Write("What do you want to do?\n" +
                    "1.Add Airport\n" +
                    "2.List All airports\n" +
                    "3.Find nearest airport\n" +
                    "4.Find aiports elevation's syandart deviation\n" +
                    "5.Print Nth super-Fibonacci\n"+
                    "6.Change log delegates\n"+
                    "0.Exit\n" + "Choice: ");
                if (int.TryParse(Console.ReadLine(), out int choice))
                {
                    
                    return choice;
                }
                Console.WriteLine("Invalid input, must be numerical. Try again.");
            }
        }
        static void addAirport()
        {
            try
            {
                Console.WriteLine("Adding an Airport.");
                Console.WriteLine("Enter Airport Code");
                string code = Console.ReadLine().ToUpper();
                Console.WriteLine("Enter City");
                String city = Console.ReadLine();
                Console.WriteLine("Enter Latitude");
                double latitude= double.Parse(Console.ReadLine());
                Console.WriteLine("Enter Longitude");
                double longitude = double.Parse(Console.ReadLine());
                Console.WriteLine("Enter Elevation in Meters");
                int elevationMeters = int.Parse(Console.ReadLine());
                Airport airport = new Airport(code, city, latitude, longitude, elevationMeters);
                AirportList.Add(airport);
                SaveToFile();
                Console.WriteLine("Airport added.");
            }catch(ArgumentException ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            } catch (FormatException)
            {
              Console.WriteLine("Error: invalid numerical input");
            }
        }
        static void SaveToFile()
        {
            try
            {
                List<string> linesList = new List<string>();
                foreach (Airport airport in AirportList)
                {
                    linesList.Add(airport.ToDataString());
                }
                File.WriteAllLines(DataFileName, linesList); 
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                Console.WriteLine("Error writing file: " + ex.Message);
            }
        }
        static void ReadAirportFile()
        {
            try
            {
                if (!File.Exists(DataFileName))
                {
                    return;
                }
                string[] linesArray = File.ReadAllLines(DataFileName);
                foreach (var line in linesArray)
                {
                    Airport airport = new Airport(line);
                }
            }
            catch (Exception ex) when (ex is FormatException || ex is ArgumentException)
            {
                Console.WriteLine($"Error (skipping line): {ex.Message} \n");
            }

        }

        static void FindNearest()
        {
            ListAllAirports();
            List<GeoCoordinate> coordinates = new List<GeoCoordinate>();
            
            
            
            if (AirportList.Count <= 1)
            {
                Console.WriteLine("Not Enough Airports In Database To Comapare");
                return;
            }
            Console.WriteLine("Enter Origin Airport Code");
            string StartCode = Console.ReadLine().ToUpper();
            
            foreach (var airport in AirportList)
            {
                if (airport.Code.Equals(StartCode))
                {
                    var sLatitude = airport.Latitude;
                    var sLongitude = airport.Longitude;
                    var _sCoord = new GeoCoordinate(sLatitude, sLongitude);
                    for (int i = 0; i < AirportList.Count; i++)
                    {
                        coordinates[i] = new GeoCoordinate(AirportList[i].Latitude, AirportList[i].Longitude);
                    }
                    for(int j = 0; j < coordinates.Count; j++)
                    {
                        if(_sCoord.GetDistanceTo(coordinates[j])< (_sCoord.GetDistanceTo(coordinates[j+1]))){
                            var closest = AirportList[j];
                            Console.WriteLine($"The Closest Airport is {closest}");
                        }
                        
                    }
                }
                else
                {
                    Console.WriteLine("Invalid Input");
                }
                


            }

            
            

            
            
            
            

            
        }
        
        static void FindSdOfEl()

        {
            return;
            /*
            double gpaSumOfSquares = AirportList.Sum(s => (s.elevation - s.elevationAvg) * (s.elevation - s.elevationAvg));
            double gpaStdDev = Math.Sqrt(gpaSumOfSquares / AirportList.Count);
            Console.WriteLine("Standard deviation is {0:0.##}", gpaStdDev);
            */
        }
        
            static void SuperFib()
        {
            return;
        }
        static void ChangeLogDel()
        {
            Logging.ChooseDelegateSetup();

        }

        static List<Airport> AirportList = new List<Airport>();
        

        static void Main(string[] args)
        {
            ReadAirportFile();
            while (true)
            {
                int choice = getMenuChoice();
                switch (choice)
                {
                    case 1:
                        addAirport();
                        Logging.LogToScreen("Adding Airport");
                        break;
                    case 2:
                        ListAllAirports();
                        Logging.LogToScreen("Listing Airports");
                        break;
                    case 3:
                        FindNearest();
                        Logging.LogToScreen("Finding Nearest");
                        break;
                    case 4:
                        FindSdOfEl();
                        Logging.LogToScreen("Computing Standart Deviation");
                        break;
                    case 5:
                        SuperFib();
                        Logging.LogToScreen("SUPER FIB MODE");
                        break;
                    case 6:
                        ChangeLogDel();
                        Logging.LogToScreen("Changing Delegates");
                        break;
                    case 0:
                        SaveToFile();
                        return; 
                    default:
                        Console.WriteLine("No such option, try again.");
                        break;
                }
                Console.WriteLine();
            }

        }
    }
}
