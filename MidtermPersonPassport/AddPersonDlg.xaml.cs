﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MidtermPersonPassport
{
    /// <summary>
    /// Interaction logic for AddPersonDlg.xaml
    /// </summary>
    public partial class AddPersonDlg : Window
    {
        public AddPersonDlg()
        {
            InitializeComponent();
        }

        bool AreInputsValid()
        {
            if(tbName.Text.Length<1 || tbName.Text.Length > 100)
            {
                MessageBox.Show("Please Enter A Valid Name", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            if(int.Parse(tbAge.Text)<1 || int.Parse(tbAge.Text) > 150|| tbAge.Text== null)
            {
                MessageBox.Show("Please Enter A Valid Age", "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }

        private void DlgBtAdd_Click(object sender, RoutedEventArgs e)
        {
            if (!AreInputsValid()) { return; }
            try
            {
                Person p = new Person();
                


                p.Name = tbName.Text;
                p.Age = int.Parse(tbAge.Text);
                Globals.ctx.People.Add(p);
                Globals.ctx.SaveChanges();
                
                this.Close();


            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DlgBtCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();

        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
