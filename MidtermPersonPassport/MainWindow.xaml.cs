﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MidtermPersonPassport
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            try
            {

           
            InitializeComponent();
              Globals.ctx = new PersonPassportDbContext();
                ReloadRecords();

            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // fatal error
            }
        }

        public void ReloadRecords()
        {
            try
            {
                
                lvPeople.ItemsSource = Globals.ctx.People.ToList(); // ex SystemException
                
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void MenuItem_Add_Click(object sender, RoutedEventArgs e)
        {
            AddPersonDlg personDlg = new AddPersonDlg();
            personDlg.Owner = this;
            if(personDlg.ShowDialog()== true)
            {
                try
                {
                    string Name = personDlg.tbName.Text;
                    int Age = int.Parse(personDlg.tbAge.Text);
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show(this, "Error\n" + ex.Message, "File data error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                
                
                

            }
            ReloadRecords();
            lvPeople.Items.Refresh();
        }

        private void lvPeople_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
           
            
                PassportDlg passDlg = new PassportDlg(lvPeople.SelectedItem as Person);
                passDlg.Owner = this;
            if (passDlg.ShowDialog() == true)
            {
                try
                {
                    string PassportNo = passDlg.tbPassNo.Text;
                   // byte[] passPic = 

                
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(ex.Message, "Error Editing Record", MessageBoxButton.OK, MessageBoxImage.Warning);
                }

            }
            
        }
        public byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            return ms.ToArray();
        }
    }
}
