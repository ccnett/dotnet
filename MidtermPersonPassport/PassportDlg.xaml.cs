﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace MidtermPersonPassport
{
    /// <summary>
    /// Interaction logic for PassportDlg.xaml
    /// </summary>
    public partial class PassportDlg : Window
    {
        Person currentPerson;
        private byte[] passPic;

        public PassportDlg(Person person)
        {
            InitializeComponent();
            currentPerson = person;
            dlgLblName.Content = currentPerson.Name;
            ReloadData();
        }

        void ReloadData()
        {
            try
            {
                //tbPassNo.Text = currentPerson.PassportNo;

               // imageViewer.Source = ByteArrayToBitmapImage(currentPerson.passPic);
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void dlgBtImage_Click(object sender, RoutedEventArgs e)
        {
            var fileContent = string.Empty;
            var filePath = string.Empty;

            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.InitialDirectory = "c:\\";
            openFileDialog.Filter = "Image Files|*.jpg;*.jpeg;*.png;|All files (*.*)|*.*";
            openFileDialog.FilterIndex = 1;
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == true)
            {
                //Get the path 
                filePath = openFileDialog.FileName;

                //Read the contents of the file into a stream
                var fileStream = openFileDialog.OpenFile();



                byte[] img = imageToByteArray(System.Drawing.Image.FromFile(filePath));


                var result = MessageBox.Show(fileContent, "Are you Sure you want to import \"" + System.IO.Path.GetFileName(openFileDialog.FileName) + "\" ?", MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (result == MessageBoxResult.Yes)
                {
                    try
                    {
                        var currOwnerImage = File.ReadAllBytes(filePath);
                        tbImage.Visibility = Visibility.Hidden; // hide the text box
                        BitmapImage bitmap = LoadImage(img); // ex: SystemException
                        imageViewer.Source = bitmap;
                    }
                    catch (Exception ex) when (ex is SystemException || ex is IOException)
                    {
                        MessageBox.Show(ex.Message, "File reading failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }

                }
            }

        }
        private static BitmapImage LoadImage(byte[] imageData)
        {
            if (imageData == null || imageData.Length == 0) return null;
            var image = new BitmapImage();
            using (var mem = new MemoryStream(imageData))
            {
                mem.Position = 0;
                image.BeginInit();
                image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.UriSource = null;
                image.StreamSource = mem;
                image.EndInit();
            }
            image.Freeze();
            return image;
        }

        public  byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            return ms.ToArray();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            this.Close();

        }

        private void Save_Button_Click(object sender, RoutedEventArgs e)
        {

           /* if (!AreInputsValid())
            {
                MessageBox.Show("Input Error" , "Invalid Passport number", MessageBoxButton.OK, MessageBoxImage.Error);
            }
           */
            try
            {
                Passport p = new Passport();
                p.PassportNo = tbPassNo.Text;
                p.Photo = passPic;
                Globals.ctx.Passports.Add(p);
                Globals.ctx.SaveChanges();
                this.Close();
                
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }


            
        }

        private bool AreInputsValid()
        {
            if (!Regex.IsMatch((tbPassNo.Text.ToUpper()), @"^[A-Z]{2}[0-9]{6}}$")){
                return false;

            }
            return true;
        }

        public static BitmapImage ByteArrayToBitmapImage(byte[] array)
        {
            using (var ms = new System.IO.MemoryStream(array))
            {
                var image = new BitmapImage();
                image.BeginInit();
                image.CacheOption = BitmapCacheOption.OnLoad; // here
                image.StreamSource = ms;
                image.EndInit();
                return image;
            }
        }
    }
    }

