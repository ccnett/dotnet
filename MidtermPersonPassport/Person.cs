﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidtermPersonPassport
{
    public class Person
    {
        public int PersonId { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        [Required]
        public int Age { get; set; }


        /*
         * Association not working for image/passnumber recall
        [NotMapped]
        [ForeignKey("Passport")]
        
        public string PassportNo => passport.PassportNo;
        [NotMapped]
        [ForeignKey("Passport")]
        
        public byte[] passPic { get => passport.Photo; }
        */
       
        // public virtual Passport passport { get; set; }
    }
}
