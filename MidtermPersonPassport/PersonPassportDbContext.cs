﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MidtermPersonPassport
{
   public class PersonPassportDbContext : DbContext
    {
        const string DbName = "MidtermLastTest.mdf";
        static string DbPath = Path.Combine(Environment.CurrentDirectory, DbName);
        public PersonPassportDbContext() : base($@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename={DbPath};Integrated Security=True;Connect Timeout=30") { }
        
        public virtual DbSet<Person> People { get; set; }
        public virtual DbSet<Passport> Passports { get; set; }


        //fluent API 
        /*
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("Admin");
            modelBuilder.Entity<Person>().ToTable("Passport");
            modelBuilder.Entity<Passport>().ToTable("Person");
        }
        */

    }
}
