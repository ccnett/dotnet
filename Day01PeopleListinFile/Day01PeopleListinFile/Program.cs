﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Day01PeopleListinFile
{
    class Program
    {
         const String DataFileName = @"..\..\..\People.txt";
        static void AddPersonInfo()
        {
            try
            {
                Console.WriteLine("Adding a person.");
                Console.Write("Enter name: ");
                string name = Console.ReadLine();
                Console.Write("Enter age: ");
                int age = int.Parse(Console.ReadLine()); // ex FormatException
                Console.Write("Enter city: ");
                string city = Console.ReadLine();
                Person person = new Person(name, age, city); // ex ArgumentException
                People.Add(person);
                Console.WriteLine("Person added.");
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            catch (FormatException ex)
            {
                Console.WriteLine("Error: invalid numerical input");
            }
        }
        static void ListAllPersonsInfo()
        {
            Console.WriteLine("Listing all persons");
            foreach (Person person in People)
            {
                Console.WriteLine(person);
            }
        }
        static void FindPersonByName() {
            
            Console.WriteLine("Now who are you looking for?");
            var search = Console.ReadLine();
             
            for(var i=0;i<People.Count();i++)
            { 
            if (People[i].Name.Contains(search)) 
                 {
                     Console.WriteLine("Matching Name: " + People[i].Name);


                 }
            }
        }
        static void FindPersonYoungerThan()
        {
            try
            {
                Console.WriteLine("What's the Age Benchmark you are looking for?");
                int ageBench = int.Parse(Console.ReadLine());
                foreach(var person in People)
                {
                    if (ageBench < person.Age)
                    {
                        Console.WriteLine(person);
                    }
                    
                }
                

            }
            catch (Exception ex) when (ex is FormatException || ex is ArgumentException)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }

        static void ReadAllPeopleFromFile() {
            try
            {
                if (!File.Exists(DataFileName))
                {
                    return;
                }
                string[] lineArray = File.ReadAllLines(DataFileName);

                foreach (String line in lineArray)
                {
                    try
                    {
                        string[] data = line.Split(';');
                        if (data.Length != 3)
                        {
                            throw new FormatException("Errr: invalid number of items in line:\n" + line);
                            
                        }
                        string name = data[0];
                        int age = int.Parse(data[1]);
                        string city = data[2];
                        Person person = new Person(name, age, city);
                        People.Add(person);
                    }catch(Exception ex)when (ex is FormatException || ex is ArgumentException)
                    {
                        Console.WriteLine("Error: " + ex.Message);
                    }

                }

                
            }
            catch (Exception e) when (e is IOException || e is SystemException)
            {
                Console.WriteLine("Exception: " + e.Message);
            }
            finally
            {
                Console.WriteLine("Executing finally block.");
            }
        }

        static void SaveAllPeopleToFile() {
            try
            {
                List<String> linesList = new List<string>();
                //Pass the filepath and filename to the StreamWriter Constructor

                //StreamWriter sw = new StreamWriter(DataFileName); 
                foreach(Person person in People)
                {
                    linesList.Add($"{person.Name};{person.Age};{person.City}");
                    
                }
                File.WriteAllLines(DataFileName, linesList); // ex IOException, SystemException
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                Console.WriteLine("Error writing file: " + ex.Message);
            }
        }

        static List<Person> People = new List<Person>();

        static int getMenuChoice()
        {
            while (true)
            {
                Console.Write("What do you want to do?\n" +
                    "1.Add person info\n" +
                    "2.List persons info\n" +
                    "3.Find and list a person by name\n" +
                    "4.Find and list persons younger than age\n" +
                    "0.Exit\n" + "Choice: ");
                if (int.TryParse(Console.ReadLine(), out int choice))
                {
                    // 3 ways to get out of the loop: break, return, throw exception
                    return choice;
                }
                Console.WriteLine("Invalid input, must be numerical. Try again.");
            }
        }

        static void Main(string[] args)
        {
            ReadAllPeopleFromFile();
            while (true)
            {
                int choice = getMenuChoice();
                switch (choice)
                {
                    case 1:
                        AddPersonInfo();
                        break;
                    case 2:
                        ListAllPersonsInfo();
                        break;
                    case 3:
                        FindPersonByName();
                        break;
                    case 4:
                        FindPersonYoungerThan();
                        break;
                    case 0:
                        SaveAllPeopleToFile();
                        return; // exit program
                    default:
                        Console.WriteLine("No such option, try again.");
                        break;
                }
                Console.WriteLine();
            }


            /*
            try
            {
                Person p1 = new Person();
                //p1.Name = "Jer;ry"; // call to setter
                string n1 = p1.Name; // call to getter

                // object initializer, may not initialize all fields/properties
                Person p2 = new Person() { Name = "Eva", Age = 76 };

                string p2s = $"Person Name={p2.Name}, Age is {p2.Age}";
                Console.WriteLine(p2s);

                p1.SomeValue = 123;
                for (int i = 0; i < 5; i++)
                {
                    int val = p1.SomeValue;
                    Console.WriteLine(val);
                }

            } finally
            {
                Console.WriteLine("Press any key");
                Console.ReadKey();
            } */
        }

    }
}
