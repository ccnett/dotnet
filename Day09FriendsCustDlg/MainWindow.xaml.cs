﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day09FriendsCustDlg
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const string DataFileName = @"..\..\data.txt";
        private List<string> friendsList = new List<string>();

        public MainWindow()
        {
            LoadDataFromFile();
            InitializeComponent();
            lvFriends.ItemsSource = friendsList;
        }

        private void LoadDataFromFile()
        {
            try
            {
                if (!File.Exists(DataFileName)) return; // do nothing if the file does not exist yet
                string[] linesList = File.ReadAllLines(DataFileName);
                foreach (string line in linesList)
                {
                    try
                    {
                        String friend = line; // ex InvalidDataException
                        friendsList.Add(friend);
                    }
                    catch (InvalidDataException ex)
                    {
                        MessageBox.Show(this, "Ignoring invalid line.\n" + ex.Message, "File data error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                File.WriteAllLines(@"..\..\data.txt", linesList);
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                MessageBox.Show(this, ex.Message, "File IO error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        public void writeToFile(string fileName, List<String> friendslistToSave)
        {
            try
            {
                List<string> linesList = new List<string>();
                foreach (String f in friendslistToSave)
                {
                    linesList.Add(f.ToString());
                }
                File.WriteAllLines(fileName, linesList);
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                MessageBox.Show(this, ex.Message, "File IO error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void miEditAdd_Click(object sender, RoutedEventArgs e)
        {
            AddEditDialog dlg = new AddEditDialog();
            dlg.Owner = this;
            if (dlg.ShowDialog() == true)
            {
                // string name = dlg.NameResult;
                string name = dlg.tbName.Text;
                friendsList.Add(name);
                lvFriends.Items.Refresh();
            }
        }

        private void lvFriends_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            string item = (string) lvFriends.SelectedItem; // possible multiple selection
            if (item == null) return;
            AddEditDialog dlg = new AddEditDialog(item);
            dlg.Owner = this;
            if (dlg.ShowDialog() == true)
            {
                int index = friendsList.IndexOf(item);
                friendsList[index] = dlg.tbName.Text;
                lvFriends.Items.Refresh();
            }
        }
        
        private void MenuItem_ExportClick(object sender, RoutedEventArgs e)
        {
            var selItems = lvFriends.SelectedItems.Cast<String>().ToList();
            if (selItems.Count == 0)
            {
                MessageBox.Show(this, "Please select some items first", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog.FilterIndex = 1;
            // saveFileDialog1.RestoreDirectory = true;
            if (saveFileDialog.ShowDialog() == true)
            {
                string fileName = saveFileDialog.FileName;
                writeToFile(fileName, selItems);
                lblCursorPosition.Text="Selected Item(s) exported";
            }
        }
        private void MenuItem_ExitClick(object sender, RoutedEventArgs e)
        {
            writeToFile(DataFileName,friendsList);
            App.Current.MainWindow.Close();
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            writeToFile(DataFileName, friendsList);
        }
        private void ContextMenuItem_Click_Edit(object sender, RoutedEventArgs e)
        {
            string item = (string)lvFriends.SelectedItem; // possible multiple selection
            if (item == null) return;
            AddEditDialog dlg = new AddEditDialog(item);
            dlg.Owner = this;
            if (dlg.ShowDialog() == true)
            {
                int index = friendsList.IndexOf(item);
                friendsList[index] = dlg.tbName.Text;
                lvFriends.Items.Refresh();
            }
        }
        private void ContextMenuItem_Click_Delete(object sender, RoutedEventArgs e)
        {
            
                int index = lvFriends.SelectedIndex;
                friendsList.Remove(friendsList[index]);
                lvFriends.Items.Refresh();
            

        }
    }
}
