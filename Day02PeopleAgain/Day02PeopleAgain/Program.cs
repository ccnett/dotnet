﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02PeopleAgain
{
    class Program
    {

    
    const string DataFileName = @"..\..\..\people.txt";

    static void ReadDataFromFile()
    {
        // It is okay to read all data at once if the number of lines is reasonably small,
        // e.g. less than 10,000. Otherise use a loop to read one line at a time.
        try
        {
            string[] linesArray = File.ReadAllLines(DataFileName);
            foreach (string line in linesArray)
            {
                try
                {
                    string type = line.Split(';')[0];
                    switch (type)
                    {
                        case "Person":
                            Person p = new Person(line);
                            PeopleList.Add(p);
                            break;
                        case "Teacher":
                            Teacher t = new Teacher(line);
                            PeopleList.Add(t);
                            break;
                        case "Student":
                            Student s = new Student(line);
                            PeopleList.Add(s);
                            break;
                        default:
                            break;
                    }
                }
                catch (InvalidParameterException ex)
                {
                    Console.WriteLine($"Error parsing line: {ex.Message}\n >> {line}");
                }
            }
        }
        catch (IOException ex)
        {
            Console.WriteLine("Error reading file: " + ex.Message);
        }
        catch (SystemException ex)
        {
            Console.WriteLine("Error reading file: " + ex.Message);
        }
    }

    public static List<Person> PeopleList = new List<Person>();

    static void Main(string[] args)
    {
        try
        {
            // Person p1 = new Person(); // won't work thanks to protected access to this constructor

            ReadDataFromFile();
            foreach (Person p in PeopleList)
            {
                Console.WriteLine(p.ToString());
            }
        }
        finally
        {
            Console.Write("Press any key.");
            Console.ReadKey();
        }
    }
}
}
