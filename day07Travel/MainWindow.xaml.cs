﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace day07Travel
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<item> itemList = new List<item>();
        const string DataFileName = @"..\..\wholeFile.txt";
        public MainWindow()
        {
            InitializeComponent();
            
              
            lsItemList.ItemsSource = itemList;
        }

        public class item
        {
            string Destination;
            string Name;
            string PassNo;
            DateTime Departure;
            DateTime Arrival;

            protected item() { }

            public item(string destination, string name, string passNo, DateTime departure, DateTime arrival)
            {
                Destination = destination;
                Name = name;
                PassNo = passNo;
                Departure = departure;
                Arrival = arrival;
            }
           
            

            public override string ToString()
            {
                return String.Format($"{ Destination},{ Name},{PassNo},//{Departure.ToString("dd/MM/yyyy")}//,{Arrival.ToString("dd/MM/yyyy")}");
            }

            public virtual string ToDataString()
            {
                return $"{Destination};{Name};{PassNo};{Departure.Date};{Arrival.Date}";
            }

        }

        

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var regex = @"^[A-Z]{2}[0-9]{6}$";
            
            string destination = tbDest.Text;
            
            string name = tbName.Text;
            if (name == "" || destination == "")
            {
                MessageBox.Show(this, "ERROR empty field", "Input error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            string passNo = tbPassport.Text;
            var match = Regex.Match(passNo, regex, RegexOptions.IgnoreCase);
            if (!match.Success)
            {
                MessageBox.Show(this, "Invalid Passport number", "Input error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            DateTime departure = (DateTime)dpDepart.SelectedDate;
            if (departure == null)
            {
                MessageBox.Show(this, "Must Set A Departure Date", "Input error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            DateTime returnDate = (DateTime)dpReturn.SelectedDate;
            if (returnDate == null)
            {
                MessageBox.Show(this, "Must Set A Return Date", "Input error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }


            item item = new item(destination, name, passNo, departure, returnDate);



            itemList.Add(item);
            lsItemList.Items.Refresh();

            //clear fields 
            tbDest.Clear();
            tbName.Clear();
            tbPassport.Clear();
            dpDepart.SelectedDate = null;
            dpReturn.SelectedDate = null;




        }
        public  void WriteToFile()
        {

            try
            {
                List<string> linesList = new List<string>();
                foreach (item thing in itemList)
                {
                    linesList.Add(thing.ToDataString());
                }
                File.WriteAllLines(DataFileName, linesList);
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                Console.WriteLine("Error writing file: " + ex.Message);
            }
        }

        public void WriteSelected()
        {

            try
            {
                List<string> linesList = new List<string>();
                foreach (item thing in itemList)
                {
                    linesList.Add(thing.ToDataString());
                }
                File.WriteAllLines(DataFileName, linesList);
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                Console.WriteLine("Error writing file: " + ex.Message);
            }
        }


        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            WriteToFile();
        }
    }
}
